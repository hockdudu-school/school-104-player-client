import urls from "../models/urls";
import reload from "../reload";
import {user as userInfo} from "../models/infoHolder";
import urlsChanged from "../urlRedirector";
import login from "./login";
import {normalizeSongName, taskPoller} from "../util";

export default function upload() {
    if (!userInfo.token) {
        history.replaceState({refer: window.location.pathname, info: 'Please login before uploading a song.'}, '', urls.site.login.url);
        login();
        return;
    }

    reload('pages/upload.njk');

    let $friendlyName = $('#uf-name');
    let $urlName = $('#uf-url-name');
    let $file = $('#uf-file');
    let $image = $('#uf-image');
    let $description = $('#uf-description');
    let $date = $('#uf-date');
    let $send = $('#uf-send');

    let $error = $('#uf-error');
    let $errorInfo = $('#uf-error-info');
    let $info = $('#uf-info');

    let $uploadForm = $('#upload-form');

    $friendlyName.on('input', () => {
        $urlName.val(normalizeSongName($friendlyName.val()));
    });

    $friendlyName.focus();

    $uploadForm.on('submit', event => {

        $error.removeClass('active');
        $info.removeClass('active');

        let song =  $file.get(0).files[0];
        let image = $image.get(0).files[0];

        let data = new FormData();
        data.append('song', song);
        if (image) data.append('image', image);
        data.append('info', JSON.stringify({
            name: $urlName.val(),
            name_friendly: $friendlyName.val(),
            description: $description.val(),
            release_date: $date.val() ? $date.val() : null,
            token: userInfo.token
        }));

        $send.get(0).disabled = true;

        $.ajax({
            url: urls.api.song.upload,
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
        }).then(data => {
            $info.addClass('active');
            taskPoller(data.task_id, function successCallback(info, textStatus, jqxhr) {
                let songName = info.name;
                history.pushState({}, '', urls.site.song.url(userInfo.name, songName));
                urlsChanged();
            }, function errorCallback(jqxhr) {
                $send.get(0).disabled = false;
                $error.addClass('active');
                $errorInfo.html(`Status: ${jqxhr.status} ${jqxhr.statusText}`);
            })
        }).fail((jqxhr) => {
            $send.get(0).disabled = false;
            $error.addClass('active');
            $errorInfo.html(`Status: ${jqxhr.status} ${jqxhr.statusText}`);
        });

        event.preventDefault();
    });
}