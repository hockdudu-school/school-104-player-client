import { user as userInfo } from "../models/infoHolder";
import urlsChanged from "../urlRedirector";
import {reloadHeader} from "../reload";
import urls from "../models/urls";

export default function logout() {
    history.replaceState({info: "Goodbye!"}, '', urls.site.index.url);

    userInfo.id = '';
    userInfo.name = '';
    userInfo.token = '';

    reloadHeader();
    urlsChanged();
}