import reload, {reloadHeader} from "../reload";
import urlChanged from "../urlRedirector";
import urls from "../models/urls";
import { user as userInfo } from "../models/infoHolder";

export default function signup() {
    reload('pages/signup.njk');

    let $username = $('#sf-username');
    let $email = $('#sf-email');
    let $password = $('#sf-password');
    let $passwordRepeat = $('#sf-password-repeat');
    let $send = $('#sf-send');

    let $invalidPassword = $('#sf-invalid-password');
    let $userExists = $('#sf-user-exists');

    let $signupForm = $('#signup-form');

    $username.focus();

    $signupForm.on('submit', event => {
        $invalidPassword.removeClass('active');
        $userExists.removeClass('active');

        if ($password.val() !== $passwordRepeat.val()) {
            $invalidPassword.addClass('active');
            event.preventDefault();
            return;
        }

        $send.get(0).disabled = true;

        $.ajax({
            url: urls.api.user.create,
            method: 'POST',
            data: JSON.stringify({
                name: $username.val(),
                email: $email.val(),
                password: $password.val(),
            }),
            contentType: 'application/json; charset=utf-8'
        }).done(value => {
            history.pushState({}, '', urls.site.user.url(value.name));
            userInfo.id = value.id;
            userInfo.name = value.name;
            userInfo.token = value.token;
            reloadHeader();
            urlChanged();
        }).fail(jqxhr => {
            if (jqxhr.status === 409) {
                $userExists.addClass('active');
            } else {
                console.log(jqxhr);
            }
            $send.get(0).disabled = false;
        });

        event.preventDefault();
    });
}