import urls from "../models/urls";
import reload from "../reload";
import {getUserInfoFromId} from "../models/userCacher";
import {getTemplateAddSongToSet} from "../util";
import {convertSong} from "../dataConverter";
import {playAudio} from "../audioPlayer";

export default function song(userName, songName) {

    $.ajax({
        url: urls.api.song.name(userName, songName)
    }).done(async data => {
        let songUserInfo = await getUserInfoFromId(data.user_id);

        reload('pages/song.njk', {
            song: convertSong(data, songUserInfo),
            url_show: {
                user: true,
                song: false
            }
        });

        $('.song-preview').on('click', event => playAudio(event.target));

        $('.song-add-set button').on('click', event => getTemplateAddSongToSet(event.target.parentElement.dataset));


    }).fail(jqxhr => {
        reload('pages/404.njk', {
            error: {
                message: `${jqxhr.status}: ${jqxhr.statusText}`,
                body: jqxhr.status === 404 ? `Song "${songName}", from user "${userName}", not found` : JSON.stringify(jqxhr)
            }
        })
    });
}