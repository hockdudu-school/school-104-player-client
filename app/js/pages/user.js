import urls from "../models/urls";
import reload, {render} from "../reload";
import {user as userInfo} from "../models/infoHolder";
import {getUserInfoFromId} from "../models/userCacher";
import {convertSet, convertSong} from "../dataConverter";
import {playAudio} from "../audioPlayer";
import {getTemplateAddSongToSet} from "../util";

export default function user(userName) {
    $.ajax({
        url: urls.api.user.name(userName)
    }).done(data => {
        reload('pages/user.njk', {user_page: {name: data.name}});
        populateSongs(data.name);
        populateSets(data.name);
    }).fail(jqxhr => {
        console.log(jqxhr);
        reload('pages/404.njk', {
            error: {
                message: `${jqxhr.status}: ${jqxhr.statusText}`,
                body: jqxhr.status === 404 ? `User "${userName}" not found` : JSON.stringify(jqxhr)
            }
        });
    });
}

function populateSongs(userName) {
    let $songsHolder = $('.songs-holder');

    $.ajax({
        url: urls.api.user.songs(userName)
    }).done(async data => {
        for (let song of data) {

            let songUserInfo = await getUserInfoFromId(song.user_id);
            let info =  {
                song: convertSong(song, songUserInfo),
                url_show: {
                    song: true,
                    user: false
                }
            };

           $songsHolder.append(render('snippets/song-item.njk', info));
        }

        $('.song-add-set button').on('click', event => getTemplateAddSongToSet(event.target.parentElement.dataset));
        $('.song-preview').on('click', event => playAudio(event.target));

    })
}

function populateSets(userName) {
    let $setsHolder = $('.sets-holder');

    $.ajax({
        url: urls.api.user.sets(userName)
    }).done(async data => {
        for (let set of data) {

            console.log(set);

            let setUserInfo = await getUserInfoFromId(set.user_id);
            let info =  {
                set: convertSet(set, setUserInfo),
                url_show: {
                    set: true,
                    user: false
                }
            };

            $setsHolder.append(render('snippets/set-item.njk', info))
        }
    })
}