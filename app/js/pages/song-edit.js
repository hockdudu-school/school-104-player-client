import urls from "../models/urls";
import reload from "../reload";
import login from "./login";
import {user as userInfo} from "../models/infoHolder";
import {normalizeSongName, taskPoller} from "../util";
import urlsChanged from "../urlRedirector";

export default function songEdit(songId) {

    if (!userInfo.token) {
        history.replaceState({
            refer: window.location.pathname,
            info: 'Please login before editing a song.'
        }, '', urls.site.login.url);
        login();
        return;
    }

    let songInfoLoadPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: urls.api.song.id(songId)
        }).then(data => {
            //This is NOT the server's only safety, it server verifies the info too
            if (data.user_id !== +userInfo.id)
                reject({
                    status: 403,
                    statusText: 'Forbidden'
                });
            resolve(data);
        }).catch(err => {
            reject(err);
        });
    });

    songInfoLoadPromise.then(songData => {
        reload("pages/edit.njk", {song: songData});

        let $editForm = $('#edit-form');
        let $friendlyName = $('#ef-name');
        let $urlName = $('#ef-url-name');
        let $image = $('#ef-image');
        let $description = $('#ef-description');
        let $date = $('#ef-date');

        let $info = $('#ef-info');

        $friendlyName.on('input', () => {
            $urlName.val(normalizeSongName($friendlyName.val()));
        });

        $friendlyName.focus();

        $editForm.on('submit', event => {

            let content = {
                token: userInfo.token,
                info: {
                    name: $urlName.val(),
                    name_friendly: $friendlyName.val(),
                    description: $description.val(),
                    release_date: $date.val() ? $date.val() : null,
                }
            };

            let image =$image.get(0).files[0];

            let songInfoUpdateStatus = new Promise((resolve, reject) => {
                $.ajax({
                    url: urls.api.song.edit(songId),
                    method: 'PATCH',
                    data: JSON.stringify(content),
                    contentType: 'application/json; charset=utf-8'
                }).done(data => {
                    resolve(data);
                }).fail(err => {
                    reject(err);
                });
            });

            let songImageUpdateStatus = image ? new Promise((resolve, reject) => {
                let data = new FormData();
                data.append('token', userInfo.token);
                data.append('file', image);

                $.ajax({
                    url: urls.api.song.editImage(songId),
                    method: 'PUT',
                    data: data,
                    processData: false,
                    contentType: false,
                }).then(taskInfo => {
                    taskPoller(taskInfo.task_id, function successCallback(info) {
                        resolve(info);
                    }, function errorCallback(err) {
                        reject(err);
                    });
                }).catch(err => {
                    reject(err);
                })
            }) : Promise.resolve();

            $info.addClass('active');

            Promise.all([songInfoUpdateStatus, songImageUpdateStatus]).then(([songUpdateStatus, imageUpdateStatus]) => {
                history.pushState({}, '', urls.site.song.url(userInfo.name, songUpdateStatus.name));
                urlsChanged();
            }).catch(err => {
                reload('pages/404.njk', {
                    error: {
                        message: `${err.status}: ${err.statusText}`,
                        body: JSON.stringify(err)
                    }
                });
            });


            event.preventDefault();
        });


    }).catch(err => {
        reload('pages/404.njk', {
            error: {
                message: `${err.status}: ${err.statusText}`,
                body: err.status === 403 ? "You don't have permissions to edit this song" : JSON.stringify(err)
            }
        });
    })
}