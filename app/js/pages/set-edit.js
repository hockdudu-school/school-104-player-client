import urls from "../models/urls";
import reload from "../reload";
import {user as userInfo} from "../models/infoHolder";
import urlsChanged from "../urlRedirector";
import login from "./login";
import {normalizeSongName, taskPoller} from "../util";
import {convertSet} from "../dataConverter";

export default function setEdit(setId) {
    if (!userInfo.token) {
        history.replaceState({refer: window.location.pathname, info: 'Please login before editing a set.'}, '', urls.site.login.url);
        login();
        return;
    }

    let setInfoLoadPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: urls.api.set.id(setId)
        }).then(data => {
            //This is NOT the server's only safety, it server verifies the info too
            if (data.user_id !== +userInfo.id)
                reject({
                    status: 403,
                    statusText: 'Forbidden'
                });
            resolve(data);
        }).catch(err => {
            reject(err);
        });
    });

    setInfoLoadPromise.then(setData => {
        reload("pages/set-edit.njk", {set: convertSet(setData, userInfo)});

        let $editSetForm = $('#edit-set-form');
        let $friendlyName = $('#sf-name');
        let $urlName = $('#sf-url-name');
        let $description = $('#sf-description');
        let $date = $('#sf-date');

        $friendlyName.on('input', () => {
            $urlName.val(normalizeSongName($friendlyName.val()));
        });
        
        $editSetForm.on('submit', event => {
            
            let content = {
                token: userInfo.token,
                info: {
                    name: $urlName.val(),
                    name_friendly: $friendlyName.val(),
                    description: $description.val(),
                    release_date: $date.val() ? $date.val() : null,
                }
            };

            $.ajax({
                url: urls.api.set.edit(setId),
                method: 'PATCH',
                data: JSON.stringify(content),
                contentType: 'application/json; charset=utf-8'
            }).then(data => {
                history.pushState({}, '', urls.site.set.url(userInfo.name, data.name));
                urlsChanged();
            }).catch(err => {
                console.log(err);
            });
          
            
            
            event.preventDefault();
        });


        console.log({set: convertSet(setData, userInfo)})


    })
}