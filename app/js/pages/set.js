import urls from "../models/urls";
import reload, {render} from "../reload";
import {getUserInfoFromId} from "../models/userCacher";
import {convertSet, convertSong} from "../dataConverter";
import {getTemplateAddSongToSet} from "../util";
import {playAudio} from "../audioPlayer";
import {user as userInfo} from "../models/infoHolder";

export default function set(userName, songName) {

    $.ajax({
        url: urls.api.set.name(userName, songName)
    }).done(async setData => {
        let setUserInfo = await getUserInfoFromId(setData.user_id);

        reload('pages/set.njk', {
            set: convertSet(setData, setUserInfo)
        });

        let $songsHolder = $('.songs-holder');

        let formattedSongs = [];
        for (const song of setData.songs) {
            let songUserInfo = await getUserInfoFromId(song.user_id);


            formattedSongs.push(convertSong(song, songUserInfo));

            let info =  {
                song: convertSong(song, songUserInfo),
                url_show: {
                    song: true,
                    user: false
                },
                set_info: {
                    id: setData.id
                }
            };

            $songsHolder.append(render('snippets/song-item.njk', info));
        }


        $('.song-add-set button').on('click', event => getTemplateAddSongToSet(event.target.parentElement.dataset));
        $('.song-preview').on('click', event => playAudio(event.target));
        $('.song-remove-set button').on('click', event => removeSongFromSet(event))
    }).fail(jqxhr => {
        reload('pages/404.njk', {error: { message: `${jqxhr.status}: ${jqxhr.statusText}`}})
    });
}

function removeSongFromSet(event) {
    let setId = event.target.parentElement.dataset["setId"];
    let songId = event.target.parentElement.dataset["songId"];

    let options = {
        token: userInfo.token,
        songs: [songId]
    };

    $.ajax({
        url: urls.api.set.editSongs(setId),
        method: 'DELETE',
        data: JSON.stringify(options),
        contentType: 'application/json; charset=utf-8'
    }).then(data => {
        $(event.target).closest('.song-item').remove();
    }).fail(jqxhr => {
        //TODO: show error
        $(event.target).closest('.song-item').remove();
    });
}