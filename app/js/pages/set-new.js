import urls from "../models/urls";
import reload from "../reload";
import {user as userInfo} from "../models/infoHolder";
import urlsChanged from "../urlRedirector";
import login from "./login";
import {normalizeSongName} from "../util";

export default function setNew() {
    if (!userInfo.token) {
        history.replaceState({refer: window.location.pathname, info: 'Please login before creating a set.'}, '', urls.site.login.url);
        login();
        return;
    }

    reload('pages/set-new.njk');

    let $friendlyName = $('#sf-name');
    let $urlName = $('#sf-url-name');
    let $description = $('#sf-description');
    let $date = $('#sf-date');
    let $send = $('#sf-send');

    let $error = $('#sf-error');
    let $errorInfo = $('#sf-error-info');
    let $info = $('#sf-info');

    let $setForm = $('#new-set-form');

    $friendlyName.on('input', () => {
        $urlName.val(normalizeSongName($friendlyName.val()));
    });

    $friendlyName.focus();

    $setForm.on('submit', event => {

        $error.removeClass('active');
        $info.removeClass('active');

        let content = {
            token: userInfo.token,
            info: {
                name: $urlName.val(),
                name_friendly: $friendlyName.val(),
                description: $description.val(),
                release_date: $date.val() ? $date.val() : null,
            }
        };

        $send.get(0).disabled = true;

        $.ajax({
            url: urls.api.set.new,
            method: 'POST',
            data: JSON.stringify(content),
            contentType: 'application/json; charset=utf-8'
        }).done(info => {
            let setName = info.name;
            history.pushState({}, '', urls.site.set.url(userInfo.name, setName));
            urlsChanged();
        }).fail(jqxhr => {
            $send.get(0).disabled = false;
            $error.addClass('active');
            $errorInfo.html(`Status: ${jqxhr.status} ${jqxhr.statusText}`);
        });




        event.preventDefault();
    });
}