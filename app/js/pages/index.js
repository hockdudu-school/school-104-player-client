import {user as userInfo} from "../models/infoHolder";
import urls from "../models/urls";
import urlsChanged from "../urlRedirector";

export default function index() {
    if (userInfo.id) history.replaceState({}, '', urls.site.user.url(userInfo.name));
    else {
        let info = history.state ? history.state.info : "Please login or signup";
        history.replaceState({info: info}, '', urls.site.login.url);
    }

    urlsChanged();
}