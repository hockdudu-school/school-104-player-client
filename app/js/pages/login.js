import urls from "../models/urls";
import reload, {reloadHeader} from "../reload";
import { user as userInfo } from "../models/infoHolder";
import urlsChanged from "../urlRedirector";

export default function login() {
    reload('pages/login.njk');

    let $loginForm = $('#login-form');
    let $username = $('#lf-username');
    let $password = $('#lf-password');
    let $info = $('#lf-info');
    let $invalidDisplay = $('#lf-invalid');

    if (history.state && history.state.info) {
        $info.html(history.state.info);
        $info.addClass('active');
    }

    $username.focus();

    $loginForm.on('submit', event => {
        $invalidDisplay.removeClass('active');


        $.ajax({
            url: urls.api.user.auth,
            method: 'POST',
            data: JSON.stringify({
                name: $username.val(),
                password: $password.val()
            }),
            contentType: 'application/json; charset=utf-8'
        }).then(data => {
            userInfo.id = data.id;
            userInfo.name = data.name;
            userInfo.token = data.token;

            // If the login was called from other function
            if (history.state.refer) {
                history.pushState({}, '', `${history.state.refer}`);
            } else {
                history.pushState({}, '', urls.site.user.url(userInfo.name));
            }

            reloadHeader();
            urlsChanged();
        }).fail(jqxhr => {
            if (jqxhr.status === 401) {
                $invalidDisplay.addClass('active');
            } else {
                console.log(jqxhr);
            }
        });


        event.preventDefault();
    })

}