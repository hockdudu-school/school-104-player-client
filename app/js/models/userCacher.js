import urls from "./urls";

let users = [];

function getUserInfoFromId(userId) {
    if (users.hasOwnProperty(userId)) return Promise.resolve(users[userId]);

    return new Promise((resolve, reject) => {
        $.ajax({
            url: urls.api.user.id(userId)
        }).then(data => {
            users[userId] = data;
            resolve(data);
        }).catch(err => {
            reject(err);
        });
    });
}

export { getUserInfoFromId };