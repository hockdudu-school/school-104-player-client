const baseUrl = {
    api: 'http://192.168.1.16:3000',
    site: 'http://192.168.1.16:3080'
};

const urls = {
    site: {
        base: baseUrl.site,
        files: {
            no_img: '/img/note.jpg'
        },
        user: {
            url: (name) => `/u/${name}`,
            regex: /^\/u\/(.+)$/
        },
        song: {
            url: (userName, songName) => `/u/${userName}/s/${songName}`,
            regex: /^\/u\/(.+)\/s\/(.+)$/,
            edit: {
                url: (id) => `/edit/song/${id}`,
                regex: /^\/edit\/song\/(\d+)$/
            }
        },
        set: {
            url: (userName, setName) => `/u/${userName}/p/${setName}`,
            regex: /^\/u\/(.+)\/p\/(.+)$/,
            new: {
                url: `/p/new`,
                regex: /^\/p\/new$/
            },
            edit: {
                url: (id) => `/edit/set/${id}`,
                regex: /^\/edit\/set\/(\d+)$/
            }
        },
        index: {
            url: '/',
            regex: /^\/$/
        },
        signup: {
            url: '/signup',
            regex: /^\/signup$/
        },
        login: {
            url: '/login',
            regex: /^\/login$/
        },
        logout: {
            url: '/logout',
            regex: /^\/logout$/
        },
        upload: {
            url: '/upload',
            regex: /^\/upload$/
        }
    },
    api: {
        base: baseUrl.api,
        user: {
            create: `${baseUrl.api}/user/new`,
            id: (id) => `${baseUrl.api}/user/id/${id}`,
            name: (name) => `${baseUrl.api}/user/name/${name}`,
            auth: `${baseUrl.api}/auth/token`,
            songs: (name) => `${baseUrl.api}/song/name/${name}`,
            sets: (name) => `${baseUrl.api}/set/name/${name}`
        },
        song: {
            id: (id) => `${baseUrl.api}/song/id/${id}`,
            name: (user, name) => `${baseUrl.api}/song/name/${user}/${name}`,
            upload: `${baseUrl.api}/song/upload`,
            queue: (id) => `${baseUrl.api}/song/queue/${id}`,
            edit: (id) => `${baseUrl.api}/song/id/${id}`,
            editImage: (id) => `${baseUrl.api}/song/id/${id}/image`
        },
        set: {
            id: (id) => `${baseUrl.api}/set/id/${id}`,
            new: `${baseUrl.api}/set/new`,
            name: (userName, setName) => `${baseUrl.api}/set/name/${userName}/${setName}`,
            edit: (id) => `${baseUrl.api}/set/id/${id}`,
            editSongs: (id) => `${baseUrl.api}/set/id/${id}/songs`
        },
        file: (file) => `${baseUrl.api}/file/${file}`
    }
};
export default urls;