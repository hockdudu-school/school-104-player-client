export const user = {
    get id() {
        return window.localStorage.getItem('user_id');
    },
    set id(id) {
        window.localStorage.setItem('user_id', id);
    },
    get name() {
        return window.localStorage.getItem('username');
    },
    set name(name) {
        window.localStorage.setItem('username', name);
    },
    get token() {
        return window.localStorage.getItem('token');
    },
    set token(token) {
        window.localStorage.setItem('token', token);
    }
};