import urlsChanged from "./urlRedirector";

urlsChanged();

window.onpopstate = (event) => {
    urlsChanged();
};

window.addEventListener('click', changeUrl);

function changeUrl(event) {
    if (event.target.tagName !== 'A') return;
    if (event.button !== 0) return;
    if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) return;

    if (event.target.dataset.hasOwnProperty('replace'))
        history.replaceState({}, '', event.target.href);
    else history.pushState({}, '', event.target.href);

    urlsChanged();
    event.preventDefault();
}