import signup from './pages/signup';
import user from './pages/user';
import song from "./pages/song";
import upload from "./pages/upload";
import login from "./pages/login";
import index from "./pages/index";
import logout from "./pages/logout";
import songEdit from "./pages/song-edit";
import setNew from "./pages/set-new";
import set from "./pages/set";
import urls from "./models/urls";
import setEdit from "./pages/set-edit";

export default function urlsChanged() {
    let url = window.location.pathname;

    if (urls.site.index.regex.test(url)) return index();
    if (urls.site.signup.regex.test(url)) return signup();
    if (urls.site.login.regex.test(url)) return login();
    if (urls.site.logout.regex.test(url)) return logout();
    if (urls.site.upload.regex.test(url)) return upload();

    if (urls.site.song.edit.regex.test(url)) {
        let match = urls.site.song.edit.regex.exec(url);
        songEdit(match[1]);
        return;
    }

    if (urls.site.song.regex.test(url)) {
        let match = urls.site.song.regex.exec(url);
        song(match[1], match[2]);
        return;
    }

    if (urls.site.set.new.regex.test(url)) return setNew();
    if (urls.site.set.regex.test(url)) {
        let match = urls.site.set.regex.exec(url);
        set(match[1], match[2]);
        return;
    }

    if(urls.site.set.edit.regex.test(url)) {
        let match = urls.site.set.edit.regex.exec(url);
        setEdit(match[1]);
        return;
    }

    if (urls.site.user.regex.test(url)) {
        let match = urls.site.user.regex.exec(url);
        user(match[1]);
    }
}
