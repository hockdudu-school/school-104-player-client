import {user as userInfo} from "./models/infoHolder";
import urls from "./models/urls";
import {deletePopup} from "./util";

export default function reload(page, options, tag) {
    let renderedElement;
    try {
        renderedElement = render(page, options);
    } catch (e) {
        console.log(e);
        throw e;
    }
    if (tag) {
        let $tag = $(tag);
        $tag.html(renderedElement);
    } else {
        let $body = $('body');

        // If it's the first page load
        if ($body.get(0).dataset.loaded === "false") {
            const pageBase = render("base.njk", options);
            $body.html(pageBase);
            delete $body.get(0).dataset.loaded;
        }

        $('main').html(renderedElement);
    }

    return renderedElement;
}

export function reloadHeader(options) {
    reload("includes/header.njk", options, "header");
}

export function render(page, options) {
    if (!options) {
        options = {
            user: userInfo,
            urls: urls
        };
    } else {
        options["user"] = userInfo;
        options["urls"] = urls
    }

    return nunjucks.render(page, options)
}

export function reloadPopup(page, options) {
    reload(page, options, '#popup-wrapper');
    $('#popup-wrapper .popup-header').after().on('click', () => deletePopup());
}