let $playPause;
let $next;
let $progress;
let $progressWrapper;
let $volume;
let $sourceElement;
let $fpName;

let $setsHolder;
let $nextSong;

export let audio;

export function playAudio(givenElement) {
    if ($sourceElement) {
        if (givenElement === $sourceElement.get(0)) return togglePlayState();
        else $sourceElement.removeClass('playing');
    }


    if (audio) {
        audio.pause();
        audio.src = ''; //destroy download
    }

    $sourceElement = $(givenElement);
    let source = $sourceElement.data('songSource');
    let songName = $sourceElement.data('songName');

    $setsHolder = $sourceElement.closest('.songs-holder');
    if ($setsHolder) {
        if ($sourceElement.parent().next().length) {
            $nextSong = $sourceElement.parent().next().children('.song-preview');
        } else {
            $nextSong = $setsHolder.find('.song-preview').first();
        }
    }

    $progressWrapper = $('#fp-progress-wrapper');
    $playPause = $('#fp-play-pause');
    $progress = $('#fp-progress');
    $fpName = $('#fp-song-name');
    $volume = $('#fp-volume');
    $next = $('#fp-next');

    audio = new Audio(source);

    togglePlayState();
    setVolume();

    $next.off('click');
    $volume.off('input');
    $progress.off('click');
    $playPause.off('click');

    $next.on('click', () => playNext());
    $volume.on('input', () => setVolume());
    $playPause.on('click', () => togglePlayState());
    $progressWrapper.on('click', (event) => setProgress(event));

    audio.addEventListener('timeupdate', () => timeupdate());
    audio.addEventListener('ended', () => playNext());

    $fpName.html(songName);
    document.title = `${songName} - dd-player`;
}

export function setVolume() {
    audio.volume = ($volume.val())
}

function togglePlayState() {
    if (audio.paused) {
        audio.play();
        $sourceElement.addClass('playing');
    }
    else {
        audio.pause();
        $sourceElement.removeClass('playing');
    }
}

function timeupdate() {
    let audioTotal = audio.duration;
    let audioCurrent = audio.currentTime;
    let time = audioCurrent / audioTotal;
    $progress.width((time ? time : 0) * 100 + '%');
}

function setProgress(event) {
    const totalTime = audio.duration;
    console.log(event);
    const wantedRatio = (event.originalEvent.clientX - event.currentTarget.offsetLeft) / event.currentTarget.offsetWidth;
    audio.currentTime = totalTime * wantedRatio;
}

function playNext() {
    if ($nextSong.length) {
        playAudio($nextSong.get(0));
    }
}