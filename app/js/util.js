import {user as userInfo} from "./models/infoHolder";
import urls from "./models/urls";
import reload, {reloadPopup} from "./reload";

export function normalizeSongName(songName) {
    return songName.trim().replace(/\s+/g, '-').toLowerCase().replace(/[^a-z0-9\-]/g, '');
}

export function taskPoller(taskId, callbackOnSuccess, callbackOnError) {
    $.ajax({
        url: urls.api.song.queue(taskId)
    }).then((data, status, jqxhr) => {
        console.log(jqxhr.status);
        if (jqxhr.status === 202) {
            setTimeout(taskPoller, 750, taskId, callbackOnSuccess, callbackOnError);
        } else {
            callbackOnSuccess(data, status, jqxhr);
        }
    }).fail((data, textStatus, jqxhr) => {
        callbackOnError(data, textStatus, jqxhr);
    })
}

export function getTemplateAddSongToSet(songInfo) {
    $.ajax({
        url: urls.api.user.sets(userInfo.name)
    }).then(data => {

        let options = {
            song: {
                id: songInfo['songId'],
                name: songInfo['songName'],
                name_friendly: songInfo['songNameFriendly']
            },
            sets: data
        };

        reloadPopup("snippets/add-song-to-set.njk", options);

        $('button.popup-song-to-set').on('click', function (event) {
            const setId = event.target.dataset['setId'];

            const options = {
                token: userInfo.token,
                songs: [songInfo["songId"]]
            };

            $.ajax({
                url: urls.api.set.editSongs(setId),
                method: 'PUT',
                data: JSON.stringify(options),
                contentType: 'application/json; charset=utf-8'
            }).then(data => {
                deletePopup();
            })
        })

    }).fail(err => {
        console.log(err);
    })
}

export function deletePopup() {
    $('#popup-wrapper').html('');
}