import urls from "./models/urls";

export function convertSong(songInfo, userInfo) {
    return {
        id: songInfo.id,
        name: songInfo.name,
        name_friendly: songInfo.name_friendly,
        description: songInfo.description,
        release_date: songInfo.release_date,
        genre_id: songInfo.genre_id,
        genre_custom: songInfo.genre_custom,
        image: songInfo.image ? urls.api.file(songInfo.image) : null,
        tags: songInfo.tags,
        author: userInfo.name,
        author_id: userInfo.id,
        source: {
            source: urls.api.file(songInfo.location),
            type: 'audio/ogg'
        },
        url: {
            song: urls.site.song.url(userInfo.name, songInfo.name),
            user: urls.site.user.url(userInfo.name)
        }
    }
}

export function convertSet(setInfo, userInfo) {
    return {
        id: setInfo.id,
        name: setInfo.name,
        name_friendly: setInfo.name_friendly,
        description: setInfo.description,
        is_album: setInfo.is_album,
        is_private: setInfo.is_private,
        genre_id: setInfo.genre_id,
        genre_custom: setInfo.genre_custom,
        release_date: setInfo.release_date,
        author: userInfo.name,
        author_id: userInfo.id,
        tags: setInfo.tags,
        url: {
            set: urls.site.set.url(userInfo.name, setInfo.name),
            user: urls.site.user.url(userInfo.name)
        }
    }
}