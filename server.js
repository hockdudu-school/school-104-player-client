const express = require('express');
const app = express();


app.use('/js', express.static('web/js'));
app.use('/css', express.static('web/css'));
app.use('/img', express.static('web/img'));

app.get('*', (req, res) => {
    res.sendFile(`${__dirname}/web/index.html`);
});




app.listen(3080, () => {
    console.log('Server started')
});





