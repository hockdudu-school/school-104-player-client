const gulp = require('gulp');
const concat = require('gulp-concat');
const nunjucks = require('gulp-nunjucks');
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const rollup = require('rollup-stream');
const source = require('vinyl-source-stream');

gulp.task('nunjucks-precompile', () =>
    gulp.src('app/templates/**')
        .pipe(nunjucks.precompile())
        .on('error', err => console.log(err))
        .pipe(concat('pages.js'))
        .pipe(gulp.dest('web/js'))
);

gulp.task('index-copy', () => {
    gulp.src('app/web/**/*')
        .pipe(gulp.dest('web'))
});

gulp.task('sass', () => {
    gulp.src('app/scss/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('web/css'));
});

gulp.task('js', () => {
    return rollup({
        input: 'app/js/entry.js',
        format: 'iife'
    }).on('error', err => console.log(err))
        .pipe(source('script.js'))
        .pipe(gulp.dest('web/js'))
});


gulp.task('watch-sass', () => {
    watch('app/scss/**/*.scss', () => {
        gulp.src('app/scss/main.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('web/css'));
    });
});

gulp.task('watch-nunjucks', () => {
    watch('app/templates/**/*.njk', () => {
        gulp.src('app/templates/**/*.njk')
            .pipe(nunjucks.precompile())
            .on('error', err => console.log(err))
            .pipe(concat('pages.js'))
            .pipe(gulp.dest('web/js'))
    })
});

gulp.task('watch-js', () => {
    watch('app/js/**/*.js', () => {
        return rollup({
            input: 'app/js/entry.js',
            format: 'iife'
        }).on('error', err => console.log(err))
            .pipe(source('script.js'))
            .pipe(gulp.dest('web/js'))
    })
});

gulp.task('watch-index', () => {
    watch('app/web/**/*', () => {
        gulp.src('app/web/**/*')
            .pipe(gulp.dest('web'))
    })
});


gulp.task('default', ['index-copy', 'nunjucks-precompile', 'sass', 'js']);
gulp.task('watch', ['default', 'watch-sass', 'watch-nunjucks', 'watch-js', 'watch-index']);